package Demo;

import model.ITaxCalculation;
import model.TaxCalculation;
import view.Display;

import java.util.Scanner;

public class Demo {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Display display = new Display();
        ITaxCalculation taxCalculation = new TaxCalculation();
        display.attach(taxCalculation);

        while (true) {
            System.out.println("-- Tax Calculation Demo.Demo --");
            System.out.println("Insert income: ");
            double income = Double.parseDouble(scanner.nextLine());
            display.addIncome(income);
            System.out.println("Insert Tax percentage: ");
            double taxPercentage = Double.parseDouble(scanner.nextLine());
            display.setTaxPercentage(taxPercentage);
            System.out.println("-----------------END----------------------");
            System.out.println();
        }


    }

}
