package view;

import model.ITaxCalculation;

public interface IDisplay {
    void addIncome(double income);

    void setTaxPercentage(double taxPercentage);

    void attach(ITaxCalculation taxCalculation);

    void detach(ITaxCalculation taxCalculation);

    void notifyTaxCalculations();

}
