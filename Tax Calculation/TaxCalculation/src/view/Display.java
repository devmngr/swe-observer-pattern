package view;

import model.ITaxCalculation;
import model.TaxCalculation;

import java.util.ArrayList;

public class Display implements IDisplay {

    private ArrayList<ITaxCalculation> taxCalculations;

    public Display() {
        taxCalculations = new ArrayList<ITaxCalculation>();
    }

    public void addIncome(double income) {
        for (ITaxCalculation tax : taxCalculations)
            tax.addIncome(income);
        System.out.println("Income: " + income);
        notifyTaxCalculations();
    }

    public void setTaxPercentage(double taxPercentage) {
        for (ITaxCalculation tax : taxCalculations)
            tax.setTaxPercentage(taxPercentage);

        System.out.println("Tax Percentage: " + taxPercentage);
        notifyTaxCalculations();
    }

    public void attach(ITaxCalculation taxCalculation) {
        taxCalculations.add(taxCalculation);
    }

    public void detach(ITaxCalculation taxCalculation) {
        taxCalculations.remove(taxCalculation);
    }

    public void notifyTaxCalculations() {
        for (ITaxCalculation tax : taxCalculations)
            System.out.println("Tax: " + tax.getTax());
    }


}
