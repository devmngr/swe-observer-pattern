package model;

public interface ITaxCalculation {


    double getTax();

    void addIncome(double income);

    void setTaxPercentage(double taxPercentage);


}
