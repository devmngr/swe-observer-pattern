package model;

import view.IDisplay;

public class TaxCalculation implements ITaxCalculation {

    private double income, taxPercentage;

    public TaxCalculation() {
        this.income = 0;
        this.taxPercentage = 0;
    }

    public double getTax() {
        return income * taxPercentage / 100;
    }

    public void addIncome(double income) {
        this.income += income;
    }

    public void setTaxPercentage(double taxPercentage) {
        this.taxPercentage = taxPercentage;
    }


}
