public interface IAuctioneer {

    void newBid(double newBid);

    void attach(Bidder bidder);

    void detach(Bidder bidder);

    void notifyBidders();
}
