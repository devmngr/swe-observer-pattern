public class BusinessBidder implements Bidder {

    private IAuctioneer auctioneer;
    private double currentBid;

    public BusinessBidder(IAuctioneer auctioneer) {
        this.auctioneer = auctioneer;
        this.currentBid = 0;
    }

    public void bid(double bid) {
        System.out.println("Business Bidder bids " + bid);
        auctioneer.newBid(bid);
    }

    public void update(double bid) {
        this.currentBid = bid;
        System.out.println("Business Bidder updated bid: " + currentBid);
    }

    public String toString() {
        return "Business Bidder" + currentBid;
    }
}
