public class PrivateBidder implements Bidder {
    private IAuctioneer auctioneer;
    private double currentBid;

    public PrivateBidder(IAuctioneer auctioneer) {
        this.auctioneer = auctioneer;
        this.currentBid = 0;
    }

    public void bid(double bid) {
        System.out.println("Private Bidder bids " + bid);
        auctioneer.newBid(bid);
    }

    public void update(double bid) {
        this.currentBid = bid;
        System.out.println("Private Bidder updated bid: " + currentBid);
    }

    public String toString() {
        return "Private Bidder " + currentBid;
    }
}
