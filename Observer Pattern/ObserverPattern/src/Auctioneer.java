import java.util.ArrayList;

public class Auctioneer implements IAuctioneer{

    private ArrayList<Bidder> bidders;
    private double currentBid;


    public Auctioneer() {
        bidders = new ArrayList<Bidder>();
        currentBid = 0;
    }

    public void newBid(double newBid) {
        currentBid = newBid;
        System.out.println("Auctioneer: Current Highest Bid " + currentBid);
        notifyBidders();
    }

    public void attach(Bidder bidder) {
        bidders.add(bidder);
    }

    public void detach(Bidder bidder) {
        bidders.remove(bidder);
    }

    public void notifyBidders() {
        for (Bidder bidder : bidders)
            bidder.update(currentBid);
    }
}
