public interface Bidder {

    void bid(double newBid);
    void update(double bid);
}
