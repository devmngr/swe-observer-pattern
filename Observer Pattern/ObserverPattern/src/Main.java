public class Main {
    public static void main(String[] args) {


        IAuctioneer auctionner = new Auctioneer();

        Bidder businessBidder = new BusinessBidder(auctionner);
        Bidder privateBidder = new PrivateBidder(auctionner);


        auctionner.attach(businessBidder);
        auctionner.attach(privateBidder);

        for (int i = 0; i < 2000; i += 100) {
            if (i % 2 == 0)
                businessBidder.bid(i);
            else
                privateBidder.bid(i);



            System.out.println(businessBidder);
            System.out.println(privateBidder);
            System.out.println();
        }
    }


}
